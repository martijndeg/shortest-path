<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    use HasFactory;

    public function waypoints()
    {
        return $this->hasMany(Waypoint::class);
    }

    public function positionList()
    {
        return $this->belongsTo(PositionList::class);
    }
}
