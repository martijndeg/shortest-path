<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;

    protected $fillable = ['x', 'y', 'start_position'];

    public function matrix()
    {
        return $this->belongsTo(Matrix::class);
    }

    public function positionLists()
    {
        return $this->belongsToMany(PositionList::class);
    }

    private function isXMin(): bool
    {
        return $this->x === $this->matrix->min_x;
    }

    private function isXMax(): bool
    {
        return $this->x === $this->matrix->max_x;
    }

    public function isYMin(): bool
    {
        return $this->y === $this->matrix->min_y;
    }

    public function isYMax(): bool
    {
        return $this->y === $this->matrix->max_y;
    }

    public function isSame(Position $otherPosition): bool
    {
        return $this->x === $otherPosition->x && $this->y === $otherPosition->y;
    }

    public function canMoveOnXAxis(): bool
    {
        return $this->isYMin() || $this->isYMax();
    }

    public function canMoveOnYAxis(): bool
    {
        return true;
    }

    public function xPositionMatches(Position $otherPosition): bool
    {
        return $this->x === $otherPosition->x;
    }

    public function yPositionMatches(Position $otherPosition): bool
    {
        return $this->y === $otherPosition->y;
    }

    public function xPositionDifference(Position $otherPosition): int
    {
        return $this->positionDifference($otherPosition, 'x');
    }

    public function yPositionDifference(Position $otherPosition): int
    {
        return $this->positionDifference($otherPosition, 'y');
    }

    public function positionDifference(Position $otherPosition, String $axis): int
    {
        if ($this->{$axis} > $otherPosition->{$axis}) {
            $difference = 1;
        } else if ($this->{$axis} < $otherPosition->{$axis}) {
            $difference = -1;
        } else {
            $difference = 0;
        }

        return $difference;
    }
}
