<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PositionList extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'matrix_id'];

    public function matrix()
    {
        return $this->belongsTo(Matrix::class);
    }

    public function positions()
    {
        return $this->belongsToMany(Position::class)->withTimestamps();
    }

    public function route()
    {
        return $this->hasOne(Route::class);
    }
}
