<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Waypoint extends Model
{
    use HasFactory;

    protected $fillable = ['order', 'position_id', 'route_id'];

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function route()
    {
        return $this->belongsTo(Waypoint::class);
    }
}
