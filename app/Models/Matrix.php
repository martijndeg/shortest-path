<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{
    use HasFactory;

    protected $fillable = ['min_x', 'max_x', 'min_y', 'max_y'];

    public function positions()
    {
        return $this->hasMany(Position::class);
    }
}
