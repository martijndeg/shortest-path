<?php

namespace App\Helpers;

use App\Models\Position;

class LegPlanner
{
    private $startPosition;
    private $endPosition;
    private $walkerPosition;
    private $matrix;
    private $stepCounter;

    public function __construct(Position $startPosition, Position $endPosition)
    {
        $this->startPosition = $startPosition;
        $this->endPosition = $endPosition;
        $this->matrix = $startPosition->matrix;
        $this->stepCounter = 0;

        $this->walkerPosition = new Position([
            'x' => $this->startPosition->x,
            'y' => $this->startPosition->y
        ]);

        $this->walkerPosition->matrix = $this->matrix;
    }

    public function determineLength(): int
    {
        while (!$this->walkerPosition->isSame($this->endPosition)) {
            $this->step();
        }

        return $this->stepCounter;
    }

    private function step(): void
    {
        if (!$this->walkerPosition->canMoveOnXAxis()) {

            if ($this->walkerPosition->xPositionMatches($this->endPosition)) {
                if ($this->walkerPosition->y < $this->endPosition->y) {
                    $this->stepUpY();
                } else {
                    $this->stepDownY();
                }
            } else {
                $distanceFromYMin = ($this->matrix->min_y + $this->walkerPosition->y) + ($this->matrix->min_y + $this->endPosition->y);
                $distanceFromYMax = ($this->matrix->max_y - $this->walkerPosition->y) + ($this->matrix->max_y - $this->endPosition->y);

                if (($distanceFromYMin <= $distanceFromYMax)) {
                    $this->stepDownY();
                } else {
                    $this->stepUpY();
                }

            }

        } else if (!$this->walkerPosition->xPositionMatches($this->endPosition)) {
            if ($this->walkerPosition->getAttribute('x') < $this->endPosition->x) {
                $this->stepUpX();
            } else {
                $this->stepDownX();
            }
        } else {
            if ($this->walkerPosition->getAttribute('y') < $this->endPosition->y) {
                $this->stepUpY();
            } else {
                $this->stepDownY();
            }
        }
    }

    private function stepUpX(): void
    {
        $this->walkerPosition->x++;
        $this->stepCounter++;
    }

    private function stepDownX(): void
    {
        $this->walkerPosition->x--;
        $this->stepCounter++;
    }

    private function stepUpY(): void
    {
        $this->walkerPosition->y++;
        $this->stepCounter++;
    }

    private function stepDownY(): void
    {
        $this->walkerPosition->y--;
        $this->stepCounter++;
    }
}
