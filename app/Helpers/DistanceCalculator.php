<?php

namespace App\Helpers;

use App\Models\Position;
use App\Models\PositionList;

class DistanceCalculator
{
    public static function distancesBetweenPositions(PositionList $positionList, Position $startPosition): Array
    {
        $positionList->with(['matrix', 'positions']);
        $positionList = array_values($positionList->positions->all());
        $routeCalculation = $orderedRoute = [];
        $listSize = sizeof($positionList);

        for ($i = 0; $i < $listSize; $i++) {

            $distance = self::calculateDistanceBetween($startPosition, $positionList[$i]);

            if ($distance > 0) {
                $routeCalculation[] = [
                    'start_position' => $startPosition->id,
                    'end_position'   => $positionList[$i]->id,
                    'leg_length'     => $distance,
                    'start_here'     => $startPosition->start_position,
                ];
            }
//            dd($routeCalculation);

            usort($routeCalculation, function ($a, $b) {
                return $a['leg_length'] > $b['leg_length'];
            });

        }

//        var_dump($routeCalculation);
//        dd($routeCalculation);

        return $routeCalculation;
    }

    private static function calculateDistanceBetween(Position $positionA, Position $positionB)
    {
        $leg = new LegPlanner($positionA, $positionB);

        return $leg->determineLength();
    }
}
