<?php

namespace App\Helpers;

use App\Models\Position;
use App\Models\PositionList;
use App\Models\Route;
use App\Models\Waypoint;

class RouteBuilder
{
    private $positionList;
    private $remainingPositionList;
    private $route;
    private $stepIndex;
    private $lastEndPosition;
    private $remainingPositionsIds; // Bad naming

    public function __construct(PositionList $positionList)
    {
        $this->positionList = $positionList;
        $this->remainingPositionList = clone $this->positionList;
        $this->remainingPositionList->positions = clone $positionList->positions;
        $this->route = Route::create();
        $this->stepIndex = 0;
        $this->lastEndPosition = $this->remainingPositionList->positions[0]->id;

        $this->remainingPositionsIds = $this->remainingPositionList->positions->pluck('id')->toArray(); // Bad naming

        $startPosition = $this->positionList->positions->where('start_position')->first();

        $this->removeRemainingPosition($startPosition->id);

    }

    public function determineRoute(): Route
    {

        if (sizeof($this->remainingPositionList->positions) <= 0) {
            $this->createWaypoint($this->lastEndPosition);
            return $this->route;
        }

        $routeLeg = DistanceCalculator::distancesBetweenPositions($this->remainingPositionList, Position::find($this->lastEndPosition))[0];

        $this->positionList->route()->save($this->route);

        $this->createWaypoint($routeLeg['start_position']);

        $this->removeRemainingPosition($routeLeg['end_position']);

        return $this->determineRoute();
    }

    private function createWaypoint($position)
    {
        $this->route->waypoints()->save(Waypoint::create([
            'order'       => $this->stepIndex,
            'position_id' => $position,
            'route_id'    => $this->route->id,
        ]));

        $this->stepIndex++;
    }

    private function removeRemainingPosition($endPosition)
    {
        $key = array_search($endPosition, $this->remainingPositionsIds);

        if ($key !== false) {
            $this->lastEndPosition = $this->remainingPositionList->positions[$key]->id;
            unset($this->remainingPositionList->positions[$key]);
            unset($this->remainingPositionsIds[$key]);
        }
    }
}
