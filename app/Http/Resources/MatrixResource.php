<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MatrixResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'min_x'     => $this->min_x,
            'max_x'     => $this->max_x,
            'min_y'     => $this->min_y,
            'max_y'     => $this->max_y,
            'positions' => new PositionCollection($this->positions),
        ];
    }
}
