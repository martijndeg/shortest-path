<?php

namespace App\Http\Controllers;

use App\Helpers\DistanceCalculator;
use App\Helpers\RouteBuilder;
use App\Models\Position;
use App\Models\PositionList;

class RouteController extends Controller
{
    public function generateRoute()
    {
        $positionList = PositionList::find(1);

        $routeBuilder = new RouteBuilder($positionList);

        $route = $routeBuilder->determineRoute();

        echo '<table>';
        echo '<thead>';
        echo '<tr><th>Order</th><th>Position</th><th>x</th><th>y</th></tr>';
        echo '</thead>';
        echo '<tbody>';

        foreach ($route->waypoints as $waypoint) {
            echo '<tr>';
            echo '<td>' . $waypoint->order . '</td>';
            echo '<td>' . $waypoint->position_id . '</td>';
            echo '<td>' . $waypoint->position->x . '</td>';
            echo '<td>' . $waypoint->position->y . '</td>';
            echo '</tr>';
        }

        echo '</tbody>';
        echo '</table>';

        dd($route);

//        var_dump($positionList);
//
//        $routeLegs = DistanceCalculator::distancesBetweenPositions($positionList);
//
////        var_dump($routeLegs);
//
//        $positionIds = collect($routeLegs)->pluck('start_position')->toArray();
//
//        $routeOrder = array_values(array_unique($positionIds));
//
////        var_dump($routeOrder);
//
//
//        $route = [];
//
//        $startPosition = Position::find($routeOrder[0]);
//
//        echo 'Starting at position: (' . $startPosition->x . ', ' . $startPosition->y . ')<br>';
//
//        for ($i = 1; $i < sizeof($routeOrder); $i++) {
//
//            $position = Position::find($routeOrder[$i]);
//            $route[] = $position;
//
//            echo 'Go to the next position at: (' . $position->x . ', ' . $position->y . ')<br>';
//        }

//        var_dump($route);
    }
}
