<?php

namespace App\Http\Controllers;

use App\Http\Resources\MatrixCollection;
use App\Models\Matrix;
use Illuminate\Http\Request;

class MatrixController extends Controller
{
    public function index()
    {
        return new MatrixCollection(Matrix::all());
    }
}
