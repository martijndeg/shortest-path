<?php

namespace Database\Seeders;

use App\Models\Matrix;
use App\Models\Position;
use Illuminate\Database\Seeder;

class PointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $matrix = Matrix::create([
            'min_x' => 0,
            'max_x' => 6,
            'min_y' => 0,
            'max_y' => 4,
        ]);

        $matrix->positions()->save(Position::create([
            'x'              => 0,
            'y'              => 0,
            'start_position' => 1,
        ]));

        $matrix->positions()->save(Position::create([
            'x' => 0,
            'y' => 1,
        ]));

        $matrix->positions()->save(Position::create([
            'x' => 1,
            'y' => 2,
        ]));

        $matrix->positions()->save(Position::create([
            'x' => 4,
            'y' => 3,
        ]));

        $matrix->positions()->save(Position::create([
            'x' => 6,
            'y' => 1,
        ]));

        $matrix->positions()->save(Position::create([
            'x' => 3,
            'y' => 2,
        ]));

        $matrix->positions()->save(Position::create([
            'x' => 3,
            'y' => 0,
        ]));
    }
}
