<?php

namespace Database\Seeders;

use App\Models\Matrix;
use App\Models\Position;
use App\Models\PositionList;
use Illuminate\Database\Seeder;

class PositionListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positionList = PositionList::create();

        $positionList->matrix()->associate(Matrix::find(1));

        $positions = Position::all();

        foreach ($positions as $position) {
            $positionList->positions()->attach($position);
        }
    }
}
