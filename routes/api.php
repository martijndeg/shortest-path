<?php

use App\Http\Controllers\MatrixController;
use App\Http\Controllers\RouteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('matrices', [MatrixController::class, 'index']);

//Route::get('routes/{positionA}/{positionB}', [RouteController::class, 'calculate']);

Route::get('routes/list', [RouteController::class, 'generateRoute']);
